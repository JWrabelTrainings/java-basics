package pl.jwrabel.zajecia;


import java.util.Random;

/**
 * Created by jakubwrabel on 05.12.2016.
 */
public class Zajecia3_2 {
    public static void main(String[] args) {
        char[] tablica = new char[]{'a', 'b', 'c'};
        char[] odwroconaTablica = new char[tablica.length];

        for (int i = 0; i < odwroconaTablica.length; i++) {
            odwroconaTablica[i] = tablica[tablica.length - i - 1];
        }

        for (int i = 0; i < odwroconaTablica.length; i++) {
            System.out.println(odwroconaTablica[i]);
        }

        // pętla FOR-EACH (nie można modyfikować!!!)
        // szablon IntelliJ: <nazwa_tablicy>.for + TAB
        for (char znak : odwroconaTablica) {
            System.out.println(znak);
        }


        // === ZADANIE === utworzyc tablice o dlugosci 10
        // i losowych wartościach
        // znaleźć wartość maksymalną i minimalną
        // wypisać

        /// wypełnienie tablicy losowymi liczbami
        int[] tablicaLosowa = new int[10];
        for (int i = 0; i < tablicaLosowa.length; i++) {
            tablicaLosowa[i] = new Random().nextInt(1000);
        }

        // wyszukanie minimum
        int min = tablicaLosowa[0];
        for (int i = 1; i < tablicaLosowa.length; i++) {
            if (tablicaLosowa[i] < min) {
                min = tablicaLosowa[i];
            }
        }

        for (int i = 0; i < tablicaLosowa.length; i++) {
            System.out.println(tablicaLosowa[i]);
        }

        System.out.println("Wartość minimalna: " + min);


        // === ZADANIE === dodanie tablic
        int[] tab1 = new int[]{10, 20, 50};
        int[] tab2 = new int[]{15, 56, 158};
        int[] suma = new int[tab1.length];

        for (int i = 0; i < tab1.length; i++) {
//            System.out.println("tab1: " + tab1[i]);
//            System.out.println("tab2: " + tab2[i]);
//            System.out.println("Suma: " + (tab1[i] + tab2[i]));
            suma[i] = (int) (tab1[i] + tab2[i]);
        }

        for (int i = 0; i < suma.length; i++) {
            System.out.println(suma[i]);
        }

        System.out.println("------------");


        int[][] tablicaDwuwymiarowa = new int[10][10];
        int a = tablicaDwuwymiarowa[1][2];

        int[][] tablicaDwuwymiarowa2 = new int[][]{{1, 2}, {3, 4}};
        for (int x = 0; x < tablicaDwuwymiarowa2.length; x++) {
            for (int y = 0; y < tablicaDwuwymiarowa2[x].length; y++) {
                System.out.print(tablicaDwuwymiarowa2[x][y] + " ");
            }
            System.out.println();
        }

        // === zadanie === tablica dwuwymiarowa 5 x 8,
        // wypełnijcie ją losowymi do 1000


    }

}
