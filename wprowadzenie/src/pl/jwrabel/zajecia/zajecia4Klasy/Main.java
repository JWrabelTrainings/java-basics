package pl.jwrabel.zajecia.zajecia4Klasy;

public class Main {

    public static void main(String[] args) {
        Czlowiek czlowiek1 = new Czlowiek(); // utworzenie obiektu klasy Czlowiek
        czlowiek1.wiek = 20; // przypisanie wartości do pola wiek
        czlowiek1.wzrost = 1.80;
        czlowiek1.waga = 100;

        System.out.println("Czlowiek1 wiek: " + czlowiek1.wiek);
        System.out.println("Czlowiek1 wzrost: " + czlowiek1.wzrost);
        System.out.println("Czlowiek1 waga: " + czlowiek1.waga);

        czlowiek1.czesc(); // wywołanie metody czesc na obiekcie klasy Czlowiek - czlowiek1


        Czlowiek czlowiek2 = new Czlowiek();
        czlowiek2.wiek = 22;
        czlowiek2.wzrost = 1.90;
        czlowiek2.waga = 80;
        czlowiek2.czesc();

        double wiekCzlowieka = czlowiek1.dajWiek();
        double zwiekszonyWiek = czlowiek1.dajWiekPowiekszonyO(20);

        System.out.println("Czlowiek2 wiek: " + czlowiek2.wiek);
        System.out.println("Czlowiek2 wzrost: " + czlowiek2.wzrost);
        System.out.println("Czlowiek2 waga: " + czlowiek2.waga);

        Kalkulator kalkulator = new Kalkulator();

        double suma = kalkulator.dodaj(30d, 15d);
        System.out.println("Suma: " + suma);
        double roznica = kalkulator.roznica(30d, 15d);
        System.out.println("Roznica: " + roznica);

        System.out.println("Zwrocona suma: " + kalkulator.dajSume());
        kalkulator.zwiekszSume(10);
        System.out.println("Zwrocona suma: " + kalkulator.dajSume());


        Punkt przykladowyPunkt1 = new Punkt(10, 10);
        Punkt przykladowyPunkt2 = new Punkt(30, 20);
        double odleglosc = przykladowyPunkt1.odleglosc(przykladowyPunkt2);
        System.out.println("Odległość między punktami: " + odleglosc);


        // Przykład na to, że do zmiennych klas przypisywane są referencje
        Punkt punkt1 = new Punkt(3, 4);
        Punkt punkt2 = new Punkt(1, 2);
        System.out.println("X1: " + punkt1.x);
        System.out.println("Y1: " + punkt1.y);
        System.out.println("X2: " + punkt2.x);
        System.out.println("Y2: " + punkt2.y);
        punkt2 = punkt1;
        System.out.println("-------------------");
        System.out.println("X1: " + punkt1.x);
        System.out.println("Y1: " + punkt1.y);
        System.out.println("X2: " + punkt2.x);
        System.out.println("Y2: " + punkt2.y);
        punkt1.x = 1000;
        System.out.println("-------------------");
        System.out.println("X1: " + punkt1.x);
        System.out.println("Y1: " + punkt1.y);
        System.out.println("X2: " + punkt2.x);
        System.out.println("Y2: " + punkt2.y);


    }
}
