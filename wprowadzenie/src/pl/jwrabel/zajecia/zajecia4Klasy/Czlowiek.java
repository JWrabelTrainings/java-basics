package pl.jwrabel.zajecia.zajecia4Klasy;

public class Czlowiek {
    // POLA KLASY
    int wiek;
    double wzrost;
    double waga;


    // METODY
    // Metoda nie zwracająca żadnej wartości (VOID)
    void czesc() {
        System.out.println("Cześć ktoś o wieku: " + wiek);
    }

    // Metoda zwracająca wartość typu double
    double dajWiek() {
        return wiek;
    }

    // Metoda zwracająca wartość typu double
    // i przyjmująca jeden parametr typu double o nazwie x
    double dajWiekPowiekszonyO(int x) {
        return wiek + x;
    }
}
