package pl.jwrabel.zajecia.zajecia4Klasy;

/**
 * Created by jakubwrabel on 06.12.2016.
 */
public class Punkt {
    double x;
    double y;

    // Konstruktor bez parametrów (domyślny gdy nie ma żadnego)
    Punkt() {

    }

    // Konstruktor z 2 parametrami
    Punkt(double x, double y) {
        this.x = x;
        this.y = y;
    }

    // metoda przyjmująca obiekt klasy Point i zwracająca odległość między punktami
    double odleglosc(Punkt punkt2) {
        double x1 = x;
        double y1 = y;
        double x2 = punkt2.x;
        double y2 = punkt2.y;

        return Math.sqrt(Math.pow(x2 - x1, 2)
                + Math.pow(y2 - y1, 2));
    }
}
