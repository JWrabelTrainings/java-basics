package pl.jwrabel.zajecia.zajecia4Klasy;

/**
 * Created by jakubwrabel on 06.12.2016.
 */
public class Zajecia4 {

    // === ZADANIE === tablica dwuwymiarowa 5 x 8, wypełnij losowymi liczbami do 1000
    // === ZADANIE === wypisanie jako pole szachownicy
    // === ZADANIE === utworzenie tablicy o podanych rozmiarach, wypełnienie jej pytając użytkownika
    // === ZADANIE === wypełnienie
    // 1 2 3 4 5
    // 2 3 4 5 6
    // 3 4 5 6 7
    // === ZADANIE === "odwrócenie" tablicy
    // === ZADANIE === transpozycja macierzy
    // === ZADANIE === wyznacznik macierzy 3x3
    // 1. ręcznie
    // 2. pętlą
    // === ZADANIE === mnożenie tablic dwuwymiarowych
    //
    // === ZADANIE === kółko i krzyżyk
    // 0. Wypisanie planszy
    // 1. wczytanie współrzędnych i zmiana planszy
    // 2. w kółko
    // 3. rozróżnienie na graczy

    // 4. sprawdzenie zwycięstwa
    public static void main(String[] args) {

        // MNOŻENIE MACIERZY 3X3 * 3X3
        double[][] tab1 = new double[][]{{1, 2, 3}, {1, 2, 3}, {3, 3, 4}};
        double[][] tab2 = new double[][]{{3, 3, 4}, {1, 2, 3}, {5, 4, 6}};
        double[][] wynikowa = new double[tab1.length][tab1.length];

        System.out.println("Macierz tab1");
        for (int i = 0; i < tab1.length; i++) {
            double[] tablicaWewnetrzna = tab1[i];
            for (int j = 0; j < tablicaWewnetrzna.length; j++) {
                System.out.print(tablicaWewnetrzna[j] + " ");
            }
            System.out.println();
        }

        System.out.println("Macierz tab2");
        for (int i = 0; i < tab2.length; i++) {
            double[] tablicaWewnetrzna = tab2[i];
            for (int j = 0; j < tablicaWewnetrzna.length; j++) {
                System.out.print(tablicaWewnetrzna[j] + " ");
            }
            System.out.println();
        }

        System.out.println("-------------------");


        for (int i = 0; i < wynikowa.length; i++) {
            for (int j = 0; j < wynikowa[i].length; j++) {
                System.out.println("I: " + i + " J: " + j);

                double[] wierszTablicy1 = tab1[i];

                double[] kolumnaTablicy2 = new double[tab2.length];

                for (int k = 0; k < kolumnaTablicy2.length; k++) {
                    kolumnaTablicy2[k] = tab2[k][j];

                }


                double sumaIloczynow = 0;
                for (int k = 0; k < kolumnaTablicy2.length; k++) { // 0 1 2
                    double iloczyn = wierszTablicy1[k] * kolumnaTablicy2[k];
//                    sumaIloczynow = sumaIloczynow + iloczyn;
                    sumaIloczynow += iloczyn;
                }


                wynikowa[i][j] = sumaIloczynow;
            }
        }

        System.out.println("-------------------");


        System.out.println("Macierz wynikowa");
        for (int i = 0; i < wynikowa.length; i++) {
            double[] tablicaWewnetrzna = wynikowa[i];
            for (int j = 0; j < tablicaWewnetrzna.length; j++) {
                System.out.print(tablicaWewnetrzna[j] + " ");
            }
            System.out.println();
        }

    }
}
