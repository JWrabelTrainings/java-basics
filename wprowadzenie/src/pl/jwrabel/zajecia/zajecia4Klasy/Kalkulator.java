package pl.jwrabel.zajecia.zajecia4Klasy;

/**
 * Created by jakubwrabel on 06.12.2016.
 */
public class Kalkulator {
    double suma;

    public static double  dodaj(double x, double y) {
        return x + y;
    }

    public static double roznica(double x, double y) {
        return x - y;
    }

    double dajSume() {
        return suma;
    }

    void zwiekszSume(double oIle) {
        suma = suma + oIle;
    }

}
