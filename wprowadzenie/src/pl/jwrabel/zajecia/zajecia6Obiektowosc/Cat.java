package pl.jwrabel.zajecia.zajecia6Obiektowosc;

/**
 * Created by jakubwrabel on 08.12.2016.
 */
public class Cat extends Animal {
    private int legsAmount;

    public int getLegsAmount() {
        return legsAmount;
    }

    public void setLegsAmount(int legsAmount) {
        this.legsAmount = legsAmount;
    }

    @Override
    public void makeSound(){
        System.out.println("MEOW");
    }
}
