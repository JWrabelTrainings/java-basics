package pl.jwrabel.zajecia.zajecia6Obiektowosc;

/**
 * Created by jakubwrabel on 08.12.2016.
 */
public interface Account {
    double getMoneyAfterYear();
    void addMoney(double amount);
}
