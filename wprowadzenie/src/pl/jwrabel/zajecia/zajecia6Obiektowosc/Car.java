package pl.jwrabel.zajecia.zajecia6Obiektowosc;

/**
 * Created by jakubwrabel on 08.12.2016.
 */
public class Car  extends Vehicle {

    public Car(String model, String make, int productionYear) {
        super(model, make, productionYear);
    }

    @Override
    public void drive() {
        System.out.println("Jadę z maksymalną prędkością 140");
    }
}
