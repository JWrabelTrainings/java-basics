package pl.jwrabel.zajecia.zajecia6Obiektowosc;

/**
 * Created by jakubwrabel on 08.12.2016.
 */
public abstract class AbstactAccount implements Account {
    protected double money;

    @Override
    public void addMoney(double amount) {
        money = money + amount;
    }
}
