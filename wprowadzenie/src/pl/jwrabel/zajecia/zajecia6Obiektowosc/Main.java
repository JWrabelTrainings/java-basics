package pl.jwrabel.zajecia.zajecia6Obiektowosc;

/**
 * Created by jakubwrabel on 08.12.2016.
 */
public class Main {
    public static void main(String[] args) {
//        String string = "Jakiś długi napis";
//        String string1 = new String("Jakiś długi napis");
//
//        int length = string.length();
//        System.out.println("Długość: " + length);
//
//        char char1 = string.charAt(0);
//        System.out.println("Char at 0: " + char1);
//
//        boolean contains = string.contains("c");
//        System.out.println("Czy zawiera napis Jakiś: " + contains);
//
//        boolean isEquals = string.equals("Jakiś długi napis");
//        System.out.println("Czy równe: " + isEquals);
//
//        boolean isEqualsIgnoringCase  = string.equalsIgnoreCase("jakiś długi napis");
//        System.out.println(isEqualsIgnoringCase);
//
//        int index = string.indexOf("długi");
//        System.out.println("IndexOf: " + index);
//
//        string = string.replaceAll("Jakiś", "Inny");
//        System.out.println(string);
//
//
//        String substring = string.substring(5);
//        System.out.println("Substring: " + substring);
//
//        String substring2 = string.substring(5, 10);
//        System.out.println("substring2: " + substring2);
//
//        String[] split = string.split(" ");
//        System.out.println(split[0]);
//        System.out.println(split[1]);
//        System.out.println(split[2]);
//
//
//        Czlowiek czlowiek = Czlowiek.createFrom("Jakub Wrabęl");
//        System.out.println(czlowiek.getImie());
//        System.out.println(czlowiek.getNazwisko());

//        printSth();
//        boolean b = returnTrue();
//        System.out.println(b);

//        System.out.println("Czy kajak jest palindromem: " + isPalindrome("kajak"));
//        System.out.println("Czy kajak2 jest palindromem: " + isPalindrome("kajak2"));
//
//        String stringFromUser = System.console().readLine();
//        System.out.println(stringFromUser);


//        Animal animal = new Animal();
//        animal.setName("Horse");
//        System.out.println(animal.getName());
//        animal.makeSound();
//
//        Cat cat = new Cat();
//        cat.setName("Mruczek");
//        System.out.println(cat.getName());
//        cat.makeSound();;
//
//        Dog dog = new Dog();
//        dog.makeSound();


//        Animal animal1 = new Animal();
//        animal1.makeSound();
//
//
//        Animal animal2 = new Cat();
//        animal2.makeSound();
//
//        Animal animal3 = new Dog();
//        animal3.makeSound();

        // KLASA VEHICLE -> dzieci Truck, Car
        // klasa vehicle model:String, make:String, productionYear:int
        // gettery, settery, konstruktor
        // void drive() w klasie vehicle - wypisuje z jaką maksymalną
        // prędkością może jechać
        // napiszcie wersje tej metody dla Truck i Car


        Vehicle vehicle = new Vehicle("", "", 2001);
        Vehicle car = new Car("Mondeo", "Ford", 2001);
        Vehicle truck = new Truck("XXXX", "Scania", 2006);

//
//        vehicle.drive();
//        car.drive();
//        truck.drive();

//        Vehicle[] vehicles = new Vehicle[]{vehicle, car, truck};
//
//        for (int i = 0; i < vehicles.length; i++) {
//            vehicles[i].drive();
//        }


        Account account1 = new PersonalAccount();
        account1.addMoney(1000);
        System.out.println("Po roku na koncie osobistym: "
                + account1.getMoneyAfterYear());

        Account account2 = new SavingsAccount();
        account2.addMoney(1000);
        System.out.println("Po roku na koncie oszczednosciowym: "
                + account2.getMoneyAfterYear());

        System.out.println(account2.toString());

    }

    public static void anything(Truck truck) {

    }

    public static void printSth() {
        System.out.println("HEllo World");
    }

    public static boolean returnTrue() {
        return true;
    }

    public static boolean isPalindrome(String string) {
        char[] chars = string.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            char charForward = chars[i];
            char charBackward = chars[chars.length - i - 1];
            if (charBackward != charForward) {
                return false;
            }
        }
        return true;

    }
}
