package pl.jwrabel.zajecia.zajecia6Obiektowosc;

/**
 * Created by jakubwrabel on 08.12.2016.
 */
public class Dog extends Animal {
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private int age;


    @Override
    public void makeSound(){
        System.out.println("BARK");
    }
}
