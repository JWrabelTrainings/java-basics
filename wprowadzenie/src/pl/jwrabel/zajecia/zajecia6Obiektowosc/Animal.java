package pl.jwrabel.zajecia.zajecia6Obiektowosc;

/**
 * Created by jakubwrabel on 08.12.2016.
 */
public class Animal {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void makeSound(){
        System.out.println("----------");
    }
}
