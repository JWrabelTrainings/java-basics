package pl.jwrabel.zajecia.zajecia6Obiektowosc;

/**
 * Created by jakubwrabel on 08.12.2016.
 */
public class SavingsAccount extends AbstactAccount {
    public static final double INTEREST_RATE = 0.05;

    @Override
    public double getMoneyAfterYear() {
        return money + money * INTEREST_RATE;
    }


    @Override
    public String toString() {
        return "Konto oszczędnościowe o stanie: " + money;
    }
}
