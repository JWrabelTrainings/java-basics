package pl.jwrabel.zajecia.zajecia5Klasy;

/**
 * Created by jakubwrabel on 07.12.2016.
 */
public class Point {
    // POLA / stan / cechy
    double x;
    double y;

    // [modyfikator_dostepu](opcjonalny) <zwracany_typ> <nazwa_metody>(<parametry>){

    //      ciało metody
    //    }

    Point(){
        System.out.println("Tworze obiekt");
        x = 1000;
        y = 1000;
    }

    Point(double nowyX, double nowyY){
        x = nowyX;
        y = nowyY;
    }

    void czesc() {
        System.out.println("Hello World!");
    }

    double dajTysiac() {
        return 1000;
    }


    double getX() {
        return x;
    }

    void setX(double nowyX) {
        x = nowyX;
    }

    double getY() {
        return y;
    }

    void setY(double nowyY) {
        y = nowyY;
    }

}
