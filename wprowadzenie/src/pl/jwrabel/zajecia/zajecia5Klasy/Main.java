package pl.jwrabel.zajecia.zajecia5Klasy;

/**
 * Created by jakubwrabel on 07.12.2016.
 */
public class Main {
    public static void main(String[] args) {
        Point point1 = new Point();
        Point point2 = new Point(400, 500);
        point1.czesc();
        System.out.println("Wartosc: " + point1.dajTysiac());



        double x = point1.getX();
        System.out.println("X: " + x);
        double y = point1.getY();
        System.out.println("Y: " + y);

        point1.x = 1000;
        point1.y = 400;

        x = point1.getX();
        System.out.println("X: " + x);
        y = point1.getY();
        System.out.println("Y: " + y);

        point1.setX(3000d);

        x = point1.getX();
        System.out.println("X: " + x);
        y = point1.getY();
        System.out.println("Y: " + y);


//        Point point2 = new Point();
//        System.out.println(point2.x);
//        System.out.println(point2.y);

    }
}
