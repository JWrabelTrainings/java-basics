package pl.jwrabel.zajecia.zajecia5Klasy;

/**
 * Created by jakubwrabel on 07.12.2016.
 */
public class Czlowiek {
    private double waga;
    private String imie;
    private String nazwisko;

    public Czlowiek(double waga, String imie, String nazwisko) {
        this.waga = waga;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public static Czlowiek createFrom(String fullname) {
        String[] split = fullname.split(" ");
        String imie = split[0];
        String nazwisko = split[1];


        Czlowiek czlowiek = new Czlowiek(80, imie, nazwisko);
        return czlowiek;
    }

    public double getWaga() {
        return waga;
    }

    public void setWaga(double waga) {
        this.waga = waga;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void printData() {
        System.out.println(imie + " " + nazwisko + " waga: " + waga);
    }
}
