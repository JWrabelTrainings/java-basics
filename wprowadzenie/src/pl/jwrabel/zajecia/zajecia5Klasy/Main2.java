package pl.jwrabel.zajecia.zajecia5Klasy;

/**
 * Created by jakubwrabel on 07.12.2016.
 */
public class Main2 {
    public static void main(String[] args) {
        // Utworzyc klase Box, x, y, z (do każdego pola getter, setter)
        // konstruktor (x, y, z) konstruktor ()

//        Box box1 = new Box();
//
//
//        Box box2 = new Box(100, 200, 300);
//        double boxVolume = box2.volume();

        // KLASA Time (hours, minutes), gettery, settery
        // konstruktor (hour, minute) konstruktor()

//        Time time = new Time(16, 23);
//        time.printTime();
//
//        time.addMinutes(100);

//        int value;
//        do {
//            value = new Scanner(System.in).nextInt();
//        }while(!time.addMinutes(value));
//
//        boolean isSuccessfull = time.addMinutes(10);
//        if(!isSuccessfull){
//            time.addMinutes(20);
//        }
//
//        time.printTime();


//        double power = Math.pow(10,2);


//        Time time = new Time(16, 23);
//        Time time2 = new Time(20, 50);
//
//        time.add(time2);
//        time2.add(time);
//        time.printTime(); // 13:13

        Czlowiek czlowiek = new Czlowiek(1000, "Jakub", "Wrabel");
        czlowiek.printData();
        String word = "BLA";
        String word2 = new String("BLA");

        System.out.println("String1: " + word);
        System.out.println("String2: " + word2);

        System.out.println("Czy są równe: " + (word.equals(word2)));


        String s1 = "BLA";
        String s2 = "BLA";

        "BLA".equals("BLA");

    }
}
