package pl.jwrabel.zajecia.kolkokrzyzyk;

/**
 * Created by jakubwrabel on 08.12.2016.
 */
public class Main {
    public static void main(String[] args) {
        Game game = new Game();
        game.start();
    }
}
