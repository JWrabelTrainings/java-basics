package pl.jwrabel.zajecia.kolkokrzyzyk;

/**
 * Created by jakubwrabel on 11.12.2016.
 */
public class IllegalFieldException extends Exception {
    private String fieldName;

    public IllegalFieldException(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }
}
