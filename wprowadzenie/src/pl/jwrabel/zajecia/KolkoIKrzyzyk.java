package pl.jwrabel.zajecia;

import java.util.Scanner;

/**
 * Created by jakubwrabel on 06.12.2016.
 */
public class KolkoIKrzyzyk {
    public static void main(String[] args) {

        // KÓŁKO I KRZYŻYK
        char[][] poleGry = new char[][]{{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};

        boolean czyRuchKrzyzyk = true;

        while (true) {
            System.out.println("==== RUCH GRACZA " + (czyRuchKrzyzyk ? 'X' : 'O'));

            System.out.println("Podaj wspolrzedna X:");
            int x = new Scanner(System.in).nextInt();

            if (x < 0 || x >= poleGry.length) {
                System.out.println("Podałeś niepoprawną wartość X");
                continue;
            }

            System.out.println("Podaj wspolrzedna Y:");
            int y = new Scanner(System.in).nextInt();

            if (y < 0 || y >= poleGry.length) {
                System.out.println("Podałeś niepoprawną wartość Y");
                continue;
            }


            if (poleGry[x][y] != ' ') {
                System.out.println("ZAJĘTE POLE!!!");
                continue;
            }

            if (czyRuchKrzyzyk) {
                poleGry[x][y] = 'X';
            } else {
                poleGry[x][y] = 'O';
            }

            // NEGACJA
            // !true == false
            // !false == true
            czyRuchKrzyzyk = !czyRuchKrzyzyk;

            // wypisanie pola gry
            for (int i = 0; i < poleGry.length; i++) {
                for (int j = 0; j < poleGry.length; j++) {
                    System.out.print("[" + poleGry[j][i] + "] ");
                }
                System.out.println();
            }
        }

    }
}
