package pl.jwrabel.zajecia.zajecia8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jakubwrabel on 12.12.2016.
 */
public class Main {

    public static final String KEY_PARZYSTE = "parzyste";
    public static final String KEY_NIEPARZYSTE = "nieparzyste";

    public static void main(String[] args) {

        Map<String, List<Integer>> map = new HashMap<>();
        map.put(KEY_PARZYSTE, new ArrayList<>());
        map.put(KEY_NIEPARZYSTE, new ArrayList<>());

        List<Integer> list = new ArrayList<>();

        for (int i = 1; i <= 100; i++) {
            list.add(i);
        }


        while (!list.isEmpty()) {
            int element = list.remove(0);

            if (element % 2 == 0) {
//                List<Integer> integers = map.get("parzyste");
//                integers.add(element);
                map.get(KEY_PARZYSTE).add(element);
            } else {
                map.get(KEY_NIEPARZYSTE).add(element);
            }
        }


        System.out.println("List size: " + list.size());

        for (Map.Entry<String, List<Integer>> entry : map.entrySet()) {
            String key = entry.getKey();
            List<Integer> value = entry.getValue();

            System.out.println(key);
            for (Integer integer : value) {
                System.out.println(integer);
            }
        }
    }
}
