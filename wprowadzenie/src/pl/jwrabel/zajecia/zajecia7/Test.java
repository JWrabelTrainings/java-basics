package pl.jwrabel.zajecia.zajecia7;

/**
 * Created by jakubwrabel on 11.12.2016.
 */
public class Test {
    public double divide(double x, double y) throws Exception {
        if(y ==0){
            throw new Exception("Nie można dzielić przez zero");
        }

        return x / y;
    }
}
