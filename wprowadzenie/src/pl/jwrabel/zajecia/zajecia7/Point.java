package pl.jwrabel.zajecia.zajecia7;

/**
 * Created by jakubwrabel on 11.12.2016.
 */
public class Point {
    private double x;
    private double y;

    public Point(double x, double y) {

        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Punkt o współrzędnych " + x + ", " + y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Point) {
            Point point = (Point) obj;

            return x == point.x && y == point.y;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return 1;
    }
}
