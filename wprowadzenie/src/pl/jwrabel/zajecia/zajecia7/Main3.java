package pl.jwrabel.zajecia.zajecia7;

import java.util.Scanner;

/**
 * Created by jakubwrabel on 11.12.2016.
 */
public class Main3 {
    public static void main(String[] args) {
        Test test = new Test();
        // wczytuje x, y
        // podaje wynik dzielenia
        // dzielenie rzuca wyjątek -> podaj jeszcze raz dzielnik

        System.out.println("Podaj X");
        double x = new Scanner(System.in).nextDouble();

        boolean shouldReplay = false;
        do {

            System.out.println("Podaj Y");
            double y = new Scanner(System.in).nextDouble();


            try {
                double divide = test.divide(x, y);
                System.out.println("Wynik dzielenia: " + divide);
                shouldReplay = false;
            } catch (Exception e) {
                shouldReplay = true;
                System.out.println("Podałeś zły dzielnik");
            }
        }while(shouldReplay);
    }

}
