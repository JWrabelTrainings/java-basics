package pl.jwrabel.zajecia.zajecia7;

import java.util.*;

/**
 * Created by jakubwrabel on 11.12.2016.
 */
public class Main {
    public static void main(String[] args) {
//        // METODA EQUALS
//        Point point1 = new Point(100, 100);
//        System.out.println(point1);
//
//        Point point2 = new Point(100, 100);
//
//        boolean areEquals = point1.equals(point2);
//        System.out.println("Czy obiekty są równe: " + areEquals);
//
//        // KLASA Client (imię, nazwisko, rok urodzenia), napiszcie equals
//        Client klient1 = new Client("Jan", "Kowalski", 1980);
//        Client klient2 = new Client("Jan", "Kowalski", 1980);
//
//        boolean areClientsEqual = klient1.equals(klient2);
//        System.out.println("Czy klienci są równi: " + areClientsEqual);
//
        // KOLEKCJE
        Set set = new HashSet();
        set.add(new Point(100, 100));
        set.add(new Point(100, 100));

//        System.out.println(set.size());

        Set<Integer> set2 = new HashSet<>();
        set2.add(1);

        // LIST
        List list = new LinkedList();
        list.add(1);
        list.add(2);
//
//        System.out.println(list.get(0));
//        System.out.println(list.get(1));
//
//        for (int i = 0; i < list.size(); i++) {
//            System.out.println(list.get(i));
//        }
//
//        for (Object o : list) {
//
//        }

        List list1 = new LinkedList();
        for (int i = 1; i <= 1000; i++) {
            list1.add(i);
        }


        for (Object o : list1) {
            System.out.println(o);
        }

        List<String> strings = new LinkedList<>();

        strings.contains("ABC");
        int size = strings.size();
        strings.add("ABC");
        String s = strings.get(0);
        Object[] objects = strings.toArray();
        String remove = strings.remove(0);
        boolean empty = strings.isEmpty();


//        long startTime = System.nanoTime();// aktualny czas w nanosekundach 10-9

        // wstaw 10 000 000 elementów do listy LinkedList<String> "ABC"


//        System.currentTimeMillis();
        List<String> testedList1 = new LinkedList<>();
        for (int i = 0; i < 10_0000; i++) {
            testedList1.add("ABC");
        }
        long startTime = System.nanoTime();
        for (int i = 0; i < testedList1.size(); i++) {
            String s1 = testedList1.get(10);
        }
        long duration = System.nanoTime() - startTime;
        System.out.println("LinkedList: " + duration / 1_000_000_000d);


        List<String> testedList2 = new ArrayList<>();
        for (int i = 0; i < 10_0000; i++) {
            testedList2.add("ABC");
        }
        startTime = System.nanoTime();
        for (int i = 0; i < testedList2.size(); i++) {
            String s1 = testedList2.get(0);
        }
        duration = System.nanoTime() - startTime;
        System.out.println("ArrayList: " + duration / 1_000_000_000d);

        // napiszcie program, który będzie się pytał użytkownika, czy dodać
        // element do listy (T/N) -> String
        // wypisanie tej listy



    }
}
