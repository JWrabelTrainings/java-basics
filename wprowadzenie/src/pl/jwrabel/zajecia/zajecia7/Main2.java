package pl.jwrabel.zajecia.zajecia7;

import java.util.*;

/**
 * Created by jakubwrabel on 11.12.2016.
 */
public class Main2 {
    public static void main(String[] args) {
//        List<String> stringList = new ArrayList<>();
//
//        while (true) {
//            System.out.println("Czy chcesz dodać kolejny element? (T/N)");
//            String answer = new Scanner(System.in).nextLine();
//
//            if (answer.equals("T")) {
//                System.out.println("Podaj treść: ");
//                String element = new Scanner(System.in).nextLine();
//                stringList.add(element);
//            } else {
//                break;
//            }
//        }
//
//        for (String element : stringList) {
//            System.out.println(element);
//        }

//        Iterator<String> iterator = stringList.iterator();
//        while(iterator.hasNext()){
//            String element = iterator.next();
//            System.out.println(element);
//        }

        // wypełnijcie listę liczbami 1-100
        // usuńcie elementy/wartości parzyste
        // wypiszcie
        // wypiscie rozmiar listy

        List<Integer> oddNumbers = new ArrayList<>();

        for (int i = 1; i <= 100; i++) {
            oddNumbers.add(i);
        }

        for (int i = 0; i < oddNumbers.size(); i++) {
            Integer integer = oddNumbers.get(i);
            if (integer % 2 == 0) {
                oddNumbers.remove(i);
            }
        }


        for (Integer oddNumber : oddNumbers) {
            System.out.println(oddNumber);
        }

        System.out.println("Rozmiar listy to: " + oddNumbers.size());


        Map<String, String> map = new HashMap<>();
        map.put("imie", "Jan");
        map.put("nazwisko", "Kowalski");
        map.put("adres", "CośTam");

        String imie = map.get("imie");
        String nazwisko = map.get("nazwisko");


        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println("Klucz");
            System.out.println(entry.getKey());
            System.out.println("Wartość");
            System.out.println(entry.getValue());
        }

        map.keySet(); // zbiór kluczy
        map.values(); // zbiór wartości

        // napiszcie program, który będzie się pytał użytkownika, czy dodać
        // element do mapy (T/N) -> String, String
        // wypisanie tej listy


        HashMap<String, String> stringHashMap = new HashMap<>();

        while (true) {
            System.out.println("Czy chcesz dodać kolejny element? (T/N)");
            String answer = new Scanner(System.in).nextLine();

            if (answer.equals("T")) {

                System.out.println("Podaj klucz: ");
                String key = new Scanner(System.in).nextLine();
                System.out.println("Podaj wartość: ");
                String value = new Scanner(System.in).nextLine();

                stringHashMap.put(key, value);
            } else {
                break;
            }
        }

        for (Map.Entry<String, String> entry : stringHashMap.entrySet()) {
            System.out.println("Klucz: " + entry.getKey()
                    + ", wartość: " + entry.getValue());
        }

    }
}
