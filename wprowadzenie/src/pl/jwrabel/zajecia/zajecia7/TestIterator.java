package pl.jwrabel.zajecia.zajecia7;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Created by jakubwrabel on 11.12.2016.
 */
public class TestIterator implements Iterable<String> {
    @Override
    public Iterator<String> iterator() {
        return null;
    }

    @Override
    public void forEach(Consumer<? super String> action) {

    }

    @Override
    public Spliterator<String> spliterator() {
        return null;
    }
}
