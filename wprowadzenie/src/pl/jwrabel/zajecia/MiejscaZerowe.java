package pl.jwrabel.zajecia;

import java.util.Scanner;

/**
 * Created by jakubwrabel on 03.12.2016.
 */
public class MiejscaZerowe {
    public static void main(String[] args) {
        System.out.println("Podaj liczbę A: ");
        double a = (new Scanner(System.in)).nextDouble();
        System.out.println("Podaj liczbę B: ");
        double b = (new Scanner(System.in)).nextDouble();
        System.out.println("Podaj liczbę C: ");
        double c = (new Scanner(System.in)).nextDouble();

        double delta = b * b - 4 * a * c;

        if (delta < 0) {
            System.out.println("Funkcja nie ma miejsc zerowych");
        } else if (delta == 0) {
            double miejsceZerowe = -b / (2 * a);
            System.out.println("Funkcja ma jedno miejsce zerowe: " + miejsceZerowe);
        } else {
            double miejsceZerowe1 = (-b - Math.sqrt(delta)) / (2 * a);
            double miejsceZerowe2 = (-b + Math.sqrt(delta)) / (2 * a);
            System.out.println("Miejsce zerowe1: " + miejsceZerowe1 + " miejsceZerowe2 " + miejsceZerowe2);
        }


        // SWITCH-CASE
        System.out.println("Podaj liczbę X: ");
        int x = (new Scanner(System.in)).nextInt();

        if (x == 1) {
            System.out.println("....");
        } else if (x == 2) {
            System.out.println("....");
        } else if (x == 3) {
            System.out.println("....");
        }

        // zmienna (int, char, String)
        switch (x) {
            case 0:
                System.out.println();
                break;
            case 1:
                System.out.println();
                break;
            default:
                System.out.println();
        }

    }
}
