package pl.jwrabel.zajecia;

import java.util.Scanner;

/**
 * Created by jakubwrabel on 05.12.2016.
 */
public class Zajecia3 {
    public static void main(String[] args) {
        // TABLICE
        // tworzenie tablicy
        // [0, 0, 0, 0, 0] 5 elementów
        int[] tablica5 = new int[5];
        // tworzenie tablicy i zainicjalizowanie przykładowymi danymi
        int[] tablica = new int[]{1, 2, 3};

        // pobranie długości tablicy
        int dlugoscTablicy = tablica.length;
        System.out.println(dlugoscTablicy);

        int el1 = tablica[0]; // pierwszy element tablicy
        int el2 = tablica[1]; // drugi element tablicy
        int el3 = tablica[2]; // trzeci element tablicy
//        System.out.println(el1);
//        System.out.println(el2);
//        System.out.println(el3);

        // dlugosc = 3 indeksy (0,1,2)
//        int el4 = tablica[3]; // pierwszy element


        double[] tablicaDouble = new double[]{4, 5, 6, 1, 2};

        for (int i = 0; i < 5; i++) { // 0, 1, 2, 3, 4
            double wartosc = tablicaDouble[i];
            System.out.println(wartosc);
        }

        System.out.println("--------------");

        tablicaDouble[0] = 10;
        for (int i = 0; i < tablicaDouble.length; i++) { // 0, 1, 2, 3, 4
            double wartosc = tablicaDouble[i];
            System.out.println(wartosc);
        }
        System.out.println("---------------");

        // szablon IntelliJ: <nazwa_tablicy>.fori + TAB
        // pętla do przechodzenia po tablicy

        // === ZADANIE === stworzyć tablicę o długości 10
        // wypełnić ją z użyciem pętli liczbami 0 .. 9
        int[] tablicaDoWypelnienia = new int[10];
        // wypełnienie
        for (int i = 0; i < tablicaDoWypelnienia.length; i++) {
            tablicaDoWypelnienia[i] = i;
        }
        // wypisanie
        for (int i = 0; i < tablicaDoWypelnienia.length; i++) {
            System.out.println(tablicaDoWypelnienia[i]);
        }
        System.out.println("----------------");

        // === ZADANIE === stworzyć tablicę o zadanej długości
        // wypełnić ją 1, -2, 3, -4, 5, -6 ....
        // wypisać

        System.out.println("Podaj długość tablicy: ");
        int dlugoscTablicy2 = new Scanner(System.in).nextInt();

        int[] tworzonaTablica = new int[dlugoscTablicy2];

        // wypełnienie
        for (int i = 0; i < tworzonaTablica.length; i++) {
            if (i % 2 == 0) {
                tworzonaTablica[i] = (i + 1);
            } else {
                tworzonaTablica[i] = -(i + 1);
            }
        }
        // wypisanie
        for (int i = 0; i < tworzonaTablica.length; i++) {
            System.out.println(tworzonaTablica[i]);
        }


        // === ZADANIE === stworzyć tablicę o zadanej długości
        // wypełnić ją kwadratami 1, 4, 9, 16, 25
        // wypisać
        int[] tablicaKwadratow = new int[10];
        // wypełnienie
        for (int i = 0; i < tablicaKwadratow.length; i++) {
            tablicaKwadratow[i] = (i + 1) * (i + 1);
        }
        // wypisanie
        for (int i = 0; i < tablicaKwadratow.length; i++) {
            System.out.println(tablicaKwadratow[i]);
        }
        System.out.println("----------------");



        // === ZADANIE === Wypełnienie tablicy znakami ASCII a-zA-Z
        int kodLiterya = 97;
        int kodLiteryz = 122;
        int kodLiteryDuzeA = 65;
        int kodLiteryDuzeZ = 90;

        char[] tablicaZnakowMalych = new char[kodLiteryz - kodLiterya + 1];
        for (int i = 0; i < tablicaZnakowMalych.length; i++) { // 0 1 2 3
            tablicaZnakowMalych[i] = (char) (kodLiterya + i);
        }

        char[] tablicaZnakowDuzych = new char[kodLiteryDuzeZ - kodLiteryDuzeA + 1];
        for (int i = 0; i < tablicaZnakowDuzych.length; i++) { // 0 1 2 3
            tablicaZnakowDuzych[i] = (char) (kodLiteryDuzeA + i);
        }

        int iloscZnakowMalych = (kodLiteryz - kodLiterya + 1);
        int iloscZnakowDuzych = (kodLiteryDuzeZ - kodLiteryDuzeA + 1);
        char[] tablicaZnakow =
                new char[iloscZnakowMalych + iloscZnakowDuzych];

        for (int i = 0; i < iloscZnakowMalych; i++) {
            tablicaZnakow[i] = (char) (kodLiterya + i);
        }

        for (int i = iloscZnakowMalych; i < tablicaZnakow.length; i++) { // 0 1 2
            tablicaZnakow[i] = (char) (kodLiteryDuzeA + i - iloscZnakowMalych);
        }

        for (int i = 0; i < tablicaZnakow.length; i++) {
            System.out.println(tablicaZnakow[i]);
        }


        System.out.println("----------------");

        // RZUTOWANIE (zamiana typu na inny typ)
//        int liczbaint = 10;
//        double liczbadouble = 20d;
//
//        liczbadouble = liczbaint;
//        liczbaint = (int)liczbadouble;
    }
}
