package pl.jwrabel.zajecia;

import java.util.Scanner;

/**
 * Created by jakubwrabel on 03.12.2016.
 */
public class Zajecia2 {

    public static void main(String[] args) {
        // OPERATOR TRÓJARGUMENTOWY
        double liczba = (new Scanner(System.in)).nextDouble();

        // wartość bezwzględna z liczby
        // Sposób 1. Za pomocą IF-a
        double wynik = 0;
        if (liczba >= 0) {
            wynik = liczba;
        } else {
            wynik = -liczba;
        }
        System.out.println("Wartość bezwzględna z X: " + liczba);

        // Sposób 2. Za pomocą operatora trójargumentowego
        double wynik2 = liczba >= 0 ? liczba : -liczba;
        System.out.println("Wartość bezwzględna z X: " + liczba);

        // składnia operatora trójargumentowego:
        // boolean ? cokolwiek : cokolwiek_o_tym_samym_typie
        // <wyrazenie> ? <wartosc_dla_true> : <wartosc_dla_false>


        // PĘTLA WHILE
        // pętla while wykonuje instrukcje w swoim ciele (pomiędzy { a } )
        // tak długo jak warunek jest spełniony == true
        // Pętlę while można rozumieć jako zapętlonego IFa

//        while(<warunek>){
//            // kod wykonywany dopóki warunek jest spełniony
//        }


        // nieskończona pętla while
        // wypisuje gwiazdki w nieskończoność, ponieważ warunek zawsze będzie spełniony == true
//        while(true){
//            System.out.println("*");
//        }

        // pętla while z licznikiem
        // pętla wykona się 4 razy (4 iteracje)
        // dla wartości licznika 0,1,2,3 (4 już nie bo 4 nie jest mniejsze od 4)
        // taki sam efekt dostaniemy, jeśli zaczniemy od licznik = 1 a warunek zmienimy na licznik <= 4
        int licznik = 0;
        while (licznik < 4) {
            System.out.println("--- KOLEJNY KROK PĘTLI (ITERACJA) ---");
            System.out.println("wartość licznika na początku iteracji: " + licznik);
            licznik++;  // To dokładnie to samo, co licznik = licznik + 1;  (zwiększenie licznika o 1)
            System.out.println("Wartość licznika na końcu iteracji (po zwiększeniu, przed ponownym sprawdzeniem warunku): " + licznik);
        }


        // === ZADANIE === pętla while wypisujące liczby parzyste od 0 do 100
        int licznikParzyste = 0;
        while (licznikParzyste <= 100) {  // pętla wykonująca się 100 razy, od wartości licznika 0 do wartości 100

            // jeśli licznik jest liczba parzystą to go wypisz
            if (licznikParzyste % 2 == 0) {
                System.out.println(licznikParzyste);
            }

            licznikParzyste++;
        }


        // pętla while wypisująca gwiazdki w nowych liniach (ilość kroków/iteracji podana przez użytkownika)
        System.out.println("Podaj liczbę gwiazdek: ");
        int ileRazy = (new Scanner(System.in)).nextInt(); // wczytanie liczby int od użytkownika
        int licznikGwiazdkiPionowo = 0;
        while (licznikGwiazdkiPionowo < ileRazy) {
            System.out.println("*");
            licznikGwiazdkiPionowo++;
        }


        // pętla while wypisująca gwiazdki w nowych liniach (ilość kroków/iteracji podana przez użytkownika)
        System.out.println("Podaj liczbę gwiazdek: ");
        int ileRazy2 = (new Scanner(System.in)).nextInt(); // wczytanie liczby int od użytkownika
        int licznikGwiazdkiPoziomo = 0;
        while (licznikGwiazdkiPoziomo < ileRazy2) {
            System.out.print("*");
            licznikGwiazdkiPoziomo++;
        }

        // === ZADANIE === Wczytać od użytkownika 2 liczby (x, y)
        // wypisać wszystkie liczby całkowite od x do y
        int liczbaX = (new Scanner(System.in)).nextInt();
        int liczbaY = (new Scanner(System.in)).nextInt();

        int licznik2 = liczbaX; // ponieważ chcemy zacząć od liczby X a nie od 0
        while (licznik2 <= liczbaY) {
            System.out.println(licznik2);
            licznik2++;
        }


        // === ZADANIE === policzyć sumę liczb od 1 do X (podane przez użytkownika)int liczbaX = (new Scanner(System.in)).nextInt();
        int liczbaXDoSumy = (new Scanner(System.in)).nextInt();

        int licznikDoSumy = 1;
        int suma = 0;
        while (licznikDoSumy <= liczbaXDoSumy) {
            System.out.println("--- KOLEJNY KROK (ITERACJA) ---");
            System.out.println("Wartość licznika: " + licznikDoSumy);
            System.out.println("Suma przed dodaniem licznika: " + suma);
            suma = suma + licznikDoSumy; // można to też zapisać jako suma += licznikXDoSumy;
            System.out.println("Suma po dodaniu licznika: " + suma);
            licznikDoSumy++;
        }
        System.out.println("Końcowa suma: " + suma);


        // === ZADANIE === Policz silnię z X
        // silnia 5! = 1*2*3*4*5
        int liczbaXDoSilni = (new Scanner(System.in)).nextInt();

        int licznikDoSilni = 1;
        int silnia = 0;
        while (licznikDoSilni <= liczbaXDoSilni) {
            System.out.println("--- KOLEJNY KROK (ITERACJA) ---");
            System.out.println("Wartość licznika: " + licznikDoSilni);
            System.out.println("Silnia przed pomnożeniem przez licznik: " + silnia);
            silnia = silnia * licznikDoSilni; // można to też zapisać jako silnia *= licznikXDoSilni;
            System.out.println("Silnia po pomnożeniu przez licznik: " + silnia);
            liczbaXDoSumy++;
        }
        System.out.println("Końcowa silnia : " + silnia);


        // INSTRUKCJE break; continue;
        // Instrukcja break; przerywa (kończy) całą pętlę

        // pętla wypisująca wartość licznika od 0 do 5 (<=)
        // dzięki zastosowaniu ifa gdy licznik == 3 wykona się instrukcja break -> przerwie (zakończy) pętlę
        // w efekcie zamiast wypisać 0,1,2,3,4,5 wypisze tylko 0,1,2,3
        int licznikDoBreak = 0;
        while (licznikDoBreak <= 5) {
            System.out.println("Licznik: " + licznikDoBreak);

            if (licznikDoBreak == 3) {
                break;
            }

            licznikDoBreak++;
        }

        // Instrukcja continue; działa podobnie do break, tylko nie przerywa wykonania całej pętli tylko kończy daną iterację i idzie do następnej
        int licznikDoContinue = 0;
        while (licznikDoContinue <= 5) {
            licznik++;

            System.out.println(licznik + "_A");
            System.out.println(licznik + "_B");
            if (licznik == 3) {
                continue;
            }
            System.out.println(licznik + "_C");
            System.out.println(licznik + "_D");
        }


        // PĘTLA DO-WHILE
        // jest to pętla bliźniacza do WHILE tylko sprawdz warunek PO ITERACJI a nie przed jak pętla while
        // czyli najpierw wykonuje kod w środku a dopiero później sprawdza warunek, jeśli jest spełniony to znowu wykonuje kod itd.
        // w efekcie wykona się przynajmniej raz
//        do {
//            // kod wykonywany w pętli
//        } while(<warunek>);

        // pętla do-while z warunkiem nigdy nie spełnionym
        // wypisze jedną gwiazkę (analogiczna petla while by nie wypisała żadnej)
        do {
            System.out.println("*");
        } while (false);


        // PĘTLA FOR
//        for(<instrukcja_poczatkowa>;<warunek>;<instrukcja_kazdego_kroku>){
//            // KOD wykonywany w pętli
//        }

        // wypisanie liczb od 1 do 10
        for (int i = 1; i <= 10; i++) {
            System.out.println("i: " + i);
        }

        // Zagnieżdżona pętla for
        // "Tabliczka mnożenia"
        for (int x = 1; x <= 10; x++) { // wykona się 10 razy

            // wykona się 10 razy w każdym kroku wyższej pętli
            for (int y = 1; y <= 10; y++) {
                System.out.println(x + " x " + y + " = " + (x * y));
            }

        }

        // === ZADANIE ===
        // pę†la for wypisująca kwadrat gwiazdek (5 x 5)
        for (int i = 0; i < 5; i++) { // pętla przechodząca po kolejnych wierszach (wykona się 5 razy)
            for (int j = 0; j < 5; j++) { // pętla wypisująca gwiazdkę (wykona się 5 razy dla każdego wiersza)
                System.out.print("*"); // wypisanie kolejnej gwiazdki w linii
            }
            System.out.println(); // przejście do nowej linii
        }


        // === ZADANIE ===
        // pę†la for wypisująca trójkąt
        // *
        // **
        // ***
        // ****
        // *****
        for (int licznikWierszy = 0; licznikWierszy <= 5; licznikWierszy++) {
            for (int licznikKolumn = 0; licznikKolumn <= 5; licznikKolumn++) { // dla każdego wiersza wypisz tyle gwiazdek, ile wynosi numer wiersza
                System.out.print("*"); // wypisanie kolejnej gwiazdki w linii
            }
            System.out.println(); // przejście do nowej linii
        }


        // Skrót IntelliJ: fori + TAB  -- stworzenie pętli for

        // === ZADANIE ===
        // wypisanie kwadratu
        // *_*_*
        // *_*_*
        // ...
        for (int licznikWierszy = 1; licznikWierszy <= 5; licznikWierszy++) { // przechodzi po wierszach
            for (int licznikKolumn = 1; licznikKolumn <= 5; licznikKolumn++) { // przechodzi po kolumnach

                // jeśli "kolumna" jest parzysta wypisz *, jeśli nieparzysta _
                if (licznikKolumn % 2 == 0) {
                    System.out.print("*");
                } else {
                    System.out.print("_");
                }

            }
            System.out.println(); // wypisanie nowej linii
        }

        // === ZADANIE === Trójkąt pod choinkę 1
        // 1: *
        // 2: ***
        // 3: *****
        // 4: *******

        for (int licznikWierszy = 1; licznikWierszy <= 5; licznikWierszy++) {
            System.out.print(licznikWierszy + ": ");
            // dla każdego wiersza (1,2,3,4) wypisz (nr_wiersza*2 - 1) gwiazdek  (1,3,5,7)
            for (int licznikKolumn = 1; licznikKolumn <= (licznikWierszy * 2 - 1); licznikKolumn++) {
                System.out.print("*");
            }
            System.out.println();
        }

        // Trójkąt pod choinkę 2   inne rozwiązdanie (zwiększanie licznika kolumn o 2)
        // 1: *
        // 3: ***
        // 5: *****
        for (int licznikWierszy = 1; licznikWierszy <= 5; licznikWierszy = licznikWierszy + 2) {
            System.out.print(licznikWierszy + ": ");
            for (int licznikKolumn = 1; licznikKolumn <= licznikWierszy; licznikKolumn++) {
                System.out.print("*");
            }
            System.out.println();
        }


        // ZADANIE: Choinka
        // 1:    *
        // 2:   ***
        // 3:  *****
        // 4: *******
        for (int licznikWierszy = 1; licznikWierszy <= 4; licznikWierszy++) {
            System.out.print(licznikWierszy + ": ");

            for (int licznikSpacji = 0; licznikSpacji < 4 - licznikWierszy; licznikSpacji++) {
                System.out.print(" ");
            }
            for (int licznikKolumn = 1; licznikKolumn <= (licznikWierszy * 2 - 1); licznikKolumn++) {
                System.out.print("*");
            }
            System.out.println();
        }

        // === ZADANIE 1 ===  Napisać program rysujący choinkę o zadanej wysokości

        int wysokosc = new Scanner(System.in).nextInt();

        for (int licznikWierszy = 1; licznikWierszy <= wysokosc; licznikWierszy++) {
            System.out.print(licznikWierszy + ": ");

            for (int licznikSpacji = 0; licznikSpacji < wysokosc - licznikWierszy; licznikSpacji++) {
                System.out.print(" ");
            }
            for (int licznikKolumn = 1; licznikKolumn <= (licznikWierszy * 2 - 1); licznikKolumn++) {
                System.out.print("*");
            }
            System.out.println();
        }

        // === ZADANIE 2 ===  Napisać program tworzący "tabliczkę działań" (x od 0 do 10, y od 0 do 10)
        // Podpowiedź wypisanie napisu "\t" tworzy tabulator (odstęp)
        // X    Y   X+Y     X-Y     X*Y     X/Y
        // 0    0    0       0       0       -
        // 0    1    1       -1      0       0
        // 0    2    2       -2      0       0
        // ....
        // 10   10   20      0      100      1


        // === ZADANIE 3 ===  Napisać program wypisujący kwadrat liczb
        // 1 2 3 4 5 6
        // 2 3 4 5 6 7
        // 3 4 5 6 7 8
        // 4 5 6 7 8 9 etc.

        // === ZADANIE 4 === Napisać program wypisujący ciąg fibonacciego od 1 do zadanego X

        // === ZADANIE 5 (*) === Napisać program rysujący romb z gwiazdek

        // === ZADANIE 6 (**) === Napisać program rysujący okrąg lub koło z gwiazdek

    }
}
