package pl.jwrabel.other;

/**
 * Created by jakubwrabel on 02.12.2016.
 */
public class Zajecia2 {
    public static void main(String[] args) {
        System.out.println("X\t|Y\t|SUMA\t|RÓŻNICA\t");
        for (int i = 0; i < 20; i++) {
            System.out.print("-");
        }
        System.out.println();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                System.out.println(i + "\t|" + j + "\t|" + (i + j) + "\t\t|" + (i - j));
            }
        }
    }
}
