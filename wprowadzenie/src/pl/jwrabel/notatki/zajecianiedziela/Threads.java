package pl.jwrabel.notatki.zajecianiedziela;

/**
 * Created by jakubwrabel on 10.12.2016.
 */
public class Threads {
    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("BLA");
            }
        };

        Thread thread1 = new ExampleThread("Wątek1", 5, 1000);
        Thread thread2 = new ExampleThread("Wątek2", 6, 300);
        thread1.start();
        thread2.start();
//
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Skończone");


    }
}
