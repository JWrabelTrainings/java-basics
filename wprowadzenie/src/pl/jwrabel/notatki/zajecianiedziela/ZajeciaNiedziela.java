package pl.jwrabel.notatki.zajecianiedziela;

/**
 * Created by jakubwrabel on 10.12.2016.
 */
public class ZajeciaNiedziela {
    public static void main(String[] args) {
        // NULL !!!
        // ENUM
        // equals
        // hashcode

        // KOLEKCJE
        // http://way2java.com/wp-content/uploads/2011/06/ss40.png
        // interface Collection<E>
        //      1. List<E>
        //          a. ArrayList
        //          b. LinkedList (jednocześnie Queue)
        //          c. Vector
        //      2. Queue<E>
        //          a. PriorityQueue
        //      3. Set<E>
        //          a. HashSet<E>
        //              I. LinkedHashSet<E>
        //          b. SortedSet<E>
        //              I. TreeSet<E>
        // interface Map<E>
        //      1. HashMap<E>

        //
        // WYJĄTKI
        //  http://www.programcreek.com/wp-content/uploads/2009/02/Exception-Hierarchy-Diagram.jpeg
        // try-catch
        // try-catch-finally
        // try-with-resources
        //
        // throw
        // throws
        //
        // WĄTKI


        // GENERYKI
        // I/O, serializacja
//        // 1
//        new BufferedReader(new FileReader(new File("/path/to/text/file.txt")));
//        -See more at:http:
////www.samouczekprogramisty.pl/operacje-na-plikach-w-jezyku-java/#sthash.XRBjmjaz.dpuf
//
//        String filePath = "/path/to/text/file.txt"
//        int number = 1234567;
//        FileWriter fileWriter = null;
//
//        try {
//            fileWriter = new FileWriter(filePath);
//            fileWriter.write(Integer.toString(number));
//        } finally {
//            if (fileWriter != null) {
//                fileWriter.close();
//            }
//        }
//
//        private static int readFromTextFile(String filePath) throws IOException {
//            BufferedReader fileReader = null;
//            try {
//                fileReader = new BufferedReader(new FileReader(filePath));
//                String number = fileReader.readLine();
//                return Integer.parseInt(number);
//            } finally {
//                if (fileReader != null) {
//                    fileReader.close();
//                }
//            }
//        }
//
//        private static void writeToTextFile(String filePath, int number) throws IOException {
//            FileWriter fileWriter = null;
//            try {
//                fileWriter = new FileWriter(filePath);
//                fileWriter.write(Integer.toString(number));
//            } finally {
//                if (fileWriter != null) {
//                    fileWriter.close();
//                }
//            }
    }
}
//}
