package pl.jwrabel.notatki.zajecianiedziela;

import java.util.*;

/**
 * Created by jakubwrabel on 10.12.2016.
 */
public class Collections {
    public static void main(String[] args) {
        // Lists
        // ArrayList
        countExecutionTime(10000000, new ArrayList());
        countExecutionTime(10000000, new LinkedList());
        countExecutionTime(10000000, new Vector());

        countElementsGettingTime(100000, new ArrayList());
        countElementsGettingTime(100000, new LinkedList());
        countElementsGettingTime(100000, new Vector());

    }

    private static void countElementsGettingTime(int elements, List list) {
        Random random = new Random();
        for (int i = 0; i < elements; i++) {
            list.add(1000);
        }

        long startTime = System.nanoTime();
        for (int i = 0; i < elements; i++) {
            list.get(random.nextInt(elements));
        }
        double diff = (System.nanoTime() - startTime)/1000000000d;
        System.out.println("Getting " + elements + " random elements from "
                + list.getClass().getName() + ": " + diff + " seconds");

    }

    public static long countExecutionTime(int elements, List list) {
        long startTime = System.nanoTime();
        for (int i = 0; i < elements; i++) {
            list.add(1000);
        }
        double diff = (System.nanoTime() - startTime)/1000000000d;
        System.out.println("Inserting " + elements + " to "
                + list.getClass().getName() + ": " + diff + " seconds");
        return startTime;
    }
}
