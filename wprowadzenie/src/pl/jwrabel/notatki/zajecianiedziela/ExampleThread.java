package pl.jwrabel.notatki.zajecianiedziela;

/**
 * Created by jakubwrabel on 10.12.2016.
 */
public class ExampleThread extends Thread {
    private int invocationsCount;
    private int sleepTime;

    public ExampleThread(String name, int invocationsCount, int sleepTime) {
        super(name);
        this.invocationsCount = invocationsCount;
        this.sleepTime = sleepTime;
    }

    @Override
    public void run() {
        for (int i = 0; i < invocationsCount; i++) {
            System.out.println("Calling thread: " + getName());
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
